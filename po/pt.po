# Portuguese translation for lightdm-gtk-greeter-settings
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the lightdm-gtk-greeter-settings package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: lightdm-gtk-greeter-settings\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-05-20 21:22-0400\n"
"PO-Revision-Date: 2015-09-10 14:47+0000\n"
"Last-Translator: David Pires <Unknown>\n"
"Language-Team: Portuguese <pt@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2017-09-20 09:52+0000\n"
"X-Generator: Launchpad (build 18449)\n"
"Language: pt\n"

#: ../lightdm-gtk-greeter-settings.desktop.in.h:1
msgid "LightDM GTK+ Greeter settings"
msgstr "Configurações do saudador LightDM GTK+"

#: ../lightdm-gtk-greeter-settings.desktop.in.h:2
msgid "Configure LightDM GTK+ Greeter"
msgstr "Configurar o saudador LightDM GTK+"

#: ../data/GtkGreeterSettingsWindow.ui.h:1
msgid "LightDM GTK+ Greeter: settings"
msgstr "Saudador LightDM GTK+: configurações"

#: ../data/GtkGreeterSettingsWindow.ui.h:2
msgctxt "option|greeter|xft-rgba"
msgid "None"
msgstr "Nenhum"

#: ../data/GtkGreeterSettingsWindow.ui.h:3
msgctxt "option|greeter|xft-rgba"
msgid "RGB"
msgstr "RGB"

#: ../data/GtkGreeterSettingsWindow.ui.h:4
msgctxt "option|greeter|xft-rgba"
msgid "BGR"
msgstr "BGR"

#: ../data/GtkGreeterSettingsWindow.ui.h:5
msgctxt "option|greeter|xft-rgba"
msgid "Vertical RGB"
msgstr "RGB Vertical"

#: ../data/GtkGreeterSettingsWindow.ui.h:6
msgctxt "option|greeter|xft-rgba"
msgid "Vertical BGR"
msgstr "BGR Vertical"

#: ../data/GtkGreeterSettingsWindow.ui.h:7
msgctxt "option|greeter|xft-hintstyle"
msgid "None"
msgstr "Nenhum"

#: ../data/GtkGreeterSettingsWindow.ui.h:8
msgctxt "option|greeter|xft-hintstyle"
msgid "Slight"
msgstr "Leve"

#: ../data/GtkGreeterSettingsWindow.ui.h:9
msgctxt "option|greeter|xft-hintstyle"
msgid "Medium"
msgstr "Médio"

#: ../data/GtkGreeterSettingsWindow.ui.h:10
msgctxt "option|greeter|xft-hintstyle"
msgid "Full"
msgstr "Pleno"

#: ../data/GtkGreeterSettingsWindow.ui.h:11
msgctxt "option|greeter|xft-antialias"
msgid "Antialias"
msgstr "Ativar suavização"

#: ../data/GtkGreeterSettingsWindow.ui.h:12
msgid "Enable this option to override system defaults"
msgstr "Habilite esta opção para substituir os padrões do sistema"

#: ../data/GtkGreeterSettingsWindow.ui.h:13
msgctxt "option|greeter|xft-dpi"
msgid "DPI"
msgstr "DPI"

#: ../data/GtkGreeterSettingsWindow.ui.h:14
msgctxt "option|greeter|xft-rgba"
msgid "Subpixel rendering"
msgstr "Renderização de subpixel"

#: ../data/GtkGreeterSettingsWindow.ui.h:15
msgctxt "option|greeter|xft-hintstyle"
msgid "Hinting"
msgstr "Alisamento"

#: ../data/GtkGreeterSettingsWindow.ui.h:16
msgid "Additional font options"
msgstr "Opções de fontes adicionais"

#: ../data/GtkGreeterSettingsWindow.ui.h:17
msgctxt "option|greeter|background"
msgid "Background"
msgstr "Fundo"

#: ../data/GtkGreeterSettingsWindow.ui.h:18
msgctxt "option|multihead"
msgid ""
" <i>(or use <a href=\"\">multihead setup</a> for individual monitors)</i>"
msgstr ""
" <i>(oo use <a href=\"\">multihead setup</a> para monitores individuais)</i>"

#: ../data/GtkGreeterSettingsWindow.ui.h:19
#: ../data/MultiheadSetupDialog.ui.h:9
msgctxt "option|greeter|background"
msgid "Color"
msgstr "Côr"

#: ../data/GtkGreeterSettingsWindow.ui.h:20
#: ../data/MultiheadSetupDialog.ui.h:8
msgctxt "option|greeter|background"
msgid "Image"
msgstr "Imagem"

#: ../data/GtkGreeterSettingsWindow.ui.h:21
msgctxt "option|greeter|theme-name"
msgid "Theme"
msgstr "Tema"

#: ../data/GtkGreeterSettingsWindow.ui.h:22
msgctxt "option|greeter|icon-theme-name"
msgid "Icons"
msgstr "Ícones"

#: ../data/GtkGreeterSettingsWindow.ui.h:23
msgctxt "option|greeter|font-name"
msgid "Font"
msgstr "Fonte"

#: ../data/GtkGreeterSettingsWindow.ui.h:24
msgctxt "option|greeter|hide-user-image"
msgid "User image"
msgstr "Imagem do utilizador"

#: ../data/GtkGreeterSettingsWindow.ui.h:25
msgctxt "option|greeter|default-user-image"
msgid "Default user image"
msgstr "Imagem de utilizador predefinida"

#: ../data/GtkGreeterSettingsWindow.ui.h:26
#: ../data/MultiheadSetupDialog.ui.h:11
msgctxt "option|greeter|user-background"
msgid "Use user wallpaper if available"
msgstr "Utilizar o papel de parede do utilizador se disponível"

#: ../data/GtkGreeterSettingsWindow.ui.h:27
msgctxt "tabs"
msgid "Appearance"
msgstr "Aparência"

#: ../data/GtkGreeterSettingsWindow.ui.h:29
#, no-c-format
msgctxt "option|greeter|clock-format"
msgid ""
"%H %I - hours, %M - minutes, %S - seconds\n"
"%d - day, %m - month, %y %Y - year\n"
"%a %A - day of the week, %b %B - month name"
msgstr ""
"%H %I - horas, %M - minutos, %S - segundos\n"
"%d - dia, %m - mês, %y %Y - ano\n"
"%a %A - dia da semana, %b %B - mês nome"

#: ../data/GtkGreeterSettingsWindow.ui.h:32
msgctxt "option|greeter|indicators"
msgid "Add indicator to list"
msgstr "Adicionar indicador à lista"

#: ../data/GtkGreeterSettingsWindow.ui.h:33
msgctxt "option|greeter|indicators"
msgid "Add"
msgstr "Adicionar"

#: ../data/GtkGreeterSettingsWindow.ui.h:34
msgctxt "option|greeter|indicators"
msgid "Remove indicator from list"
msgstr "Remover indicador da lista"

#: ../data/GtkGreeterSettingsWindow.ui.h:35
msgctxt "option|greeter|indicators"
msgid "Remove"
msgstr "Remover"

#: ../data/GtkGreeterSettingsWindow.ui.h:36
msgctxt "option|greeter|indicators"
msgid "Move up"
msgstr "Mover para cima"

#: ../data/GtkGreeterSettingsWindow.ui.h:37
msgctxt "option|greeter|indicators"
msgid "Up"
msgstr "Para cima"

#: ../data/GtkGreeterSettingsWindow.ui.h:38
msgctxt "option|greeter|indicators"
msgid "Move down"
msgstr "Mover para baixo"

#: ../data/GtkGreeterSettingsWindow.ui.h:39
msgctxt "option|greeter|indicators"
msgid "Down"
msgstr "Para baixo"

#: ../data/GtkGreeterSettingsWindow.ui.h:40
msgctxt "option|greeter|indicators"
msgid "Templates"
msgstr "Modelos"

#: ../data/GtkGreeterSettingsWindow.ui.h:41
msgctxt "option|greeter|clock-format"
msgid "Clock format:"
msgstr "Formato do relógio:"

#: ../data/GtkGreeterSettingsWindow.ui.h:42
msgctxt "option|greeter|indicators"
msgid "Redefine indicators"
msgstr "Redefinir indicadores"

#: ../data/GtkGreeterSettingsWindow.ui.h:43
msgctxt "tabs"
msgid "Panel"
msgstr "Painel"

#: ../data/GtkGreeterSettingsWindow.ui.h:44
msgctxt "option|greeter|position"
msgid "Horizontal"
msgstr "Horizontal"

#: ../data/GtkGreeterSettingsWindow.ui.h:45
msgctxt "option|greeter|position"
msgid "Vertical"
msgstr "Vertical"

#: ../data/GtkGreeterSettingsWindow.ui.h:47
#, no-c-format
msgctxt "option|greeter|position"
msgid "in %"
msgstr "em %"

#: ../data/GtkGreeterSettingsWindow.ui.h:48
msgctxt "option|greeter|position"
msgid "from right"
msgstr "a partir da direita"

#: ../data/GtkGreeterSettingsWindow.ui.h:49
msgctxt "option|greeter|position"
msgid "from bottom"
msgstr "da parte inferior"

#: ../data/GtkGreeterSettingsWindow.ui.h:50
msgctxt "option|greeter|position"
msgid "Select base point and its position."
msgstr "Selecionar ponto de base e a sua posição."

#: ../data/GtkGreeterSettingsWindow.ui.h:51
msgctxt "tabs"
msgid "Window position"
msgstr "Posição da janela"

#: ../data/GtkGreeterSettingsWindow.ui.h:52
msgctxt "option|greeter|screensaver-timeout"
msgid "Never"
msgstr "Nunca"

#: ../data/GtkGreeterSettingsWindow.ui.h:53
msgid "Accessibility"
msgstr "Acessibilidade"

#: ../data/GtkGreeterSettingsWindow.ui.h:54
msgctxt "option|greeter|reader"
msgid "Select path to reader..."
msgstr "Selecionar caminho para o leitor..."

#: ../data/GtkGreeterSettingsWindow.ui.h:55
msgctxt "option|greeter|reader"
msgid "Command to launch screen reader"
msgstr "Comando para lançar o leitor de ecrã"

#: ../data/GtkGreeterSettingsWindow.ui.h:56
msgctxt "option|greeter|keyboard"
msgid "Select path to keyboard..."
msgstr "Selecionar caminho para o teclado..."

#: ../data/GtkGreeterSettingsWindow.ui.h:57
msgctxt "option|greeter|keyboard"
msgid "Command to launch on-screen keyboard"
msgstr "Comando para lançar o teclado no ecrã"

#: ../data/GtkGreeterSettingsWindow.ui.h:58
msgctxt "option|greeter|a11y-states"
msgid "Disabled at start"
msgstr "Desativado no início"

#: ../data/GtkGreeterSettingsWindow.ui.h:59
msgctxt "option|greeter|a11y-states"
msgid "Enabled at start"
msgstr "Ativado no início"

#: ../data/GtkGreeterSettingsWindow.ui.h:60
msgctxt "option|greeter|a11y-states"
msgid "Save state between launches"
msgstr "Guardar estado entre lançamentos"

#: ../data/GtkGreeterSettingsWindow.ui.h:61
msgctxt "option|greeter|a11y-theme"
msgid "Contrast theme"
msgstr "Tema contraste"

#: ../data/GtkGreeterSettingsWindow.ui.h:62
msgctxt "option|greeter|a11y-font"
msgid "Large font"
msgstr "Fonte grande"

#: ../data/GtkGreeterSettingsWindow.ui.h:63
msgctxt "greeter|option|reader"
msgid "Reader"
msgstr "Leitor"

#: ../data/GtkGreeterSettingsWindow.ui.h:64
msgctxt "greeter|option|keyboard"
msgid "Keyboard"
msgstr "Teclado"

#: ../data/GtkGreeterSettingsWindow.ui.h:65
msgctxt "option|greeter|screensaver-timeout"
msgid "Timeout until the screen blanks "
msgstr "Tempo limite antes do ecrã desligar "

#: ../data/GtkGreeterSettingsWindow.ui.h:66
msgctxt "greeter|option|allow-debugging"
msgid "Debugging mode"
msgstr "Modo de depuração"

#: ../data/GtkGreeterSettingsWindow.ui.h:67
msgctxt "greeter|option|allow-debugging"
msgid ""
"Enable keys to launch GtkInspector\n"
"More informative log"
msgstr ""
"Ativar teclas para lançar GtkInspector\n"
"Log mais informativo"

#: ../data/GtkGreeterSettingsWindow.ui.h:69
msgctxt "tabs"
msgid "Misc."
msgstr "Div."

#: ../data/GtkGreeterSettingsWindow.ui.h:70
#: ../data/IndicatorPropertiesDialog.ui.h:3
msgctxt "button"
msgid "_Close"
msgstr "_Fechar"

#: ../data/GtkGreeterSettingsWindow.ui.h:71
msgctxt "button"
msgid "_Save"
msgstr "_Gravar"

#: ../data/GtkGreeterSettingsWindow.ui.h:72
msgctxt "button"
msgid "_Reload"
msgstr "_Recarregar"

#: ../data/GtkGreeterSettingsWindow.ui.h:73
msgid "Read configuration file"
msgstr "Ler ficheiro de configuração"

#: ../data/IconChooserDialog.ui.h:1
msgctxt "icon-dialog"
msgid "Select icon"
msgstr "Selecionar ícone"

#: ../data/IconChooserDialog.ui.h:2
msgctxt "button"
msgid "_Cancel"
msgstr "_Cancelar"

#: ../data/IconChooserDialog.ui.h:3 ../data/IndicatorPropertiesDialog.ui.h:4
#: ../data/MultiheadSetupDialog.ui.h:14
msgctxt "button"
msgid "_OK"
msgstr "_OK"

#: ../data/IconChooserDialog.ui.h:4
msgid "Add selected indicator to the list and close this window"
msgstr "Adicionar o indicador selecionado à lista e fechar esta janela"

#: ../data/IconChooserDialog.ui.h:5
msgctxt "icon-dialog"
msgid "Icon _name:"
msgstr "Ícone _nome:"

#: ../data/IconChooserDialog.ui.h:6
msgctxt "icon-dialog"
msgid "_Contexts:"
msgstr "_Contextos:"

#: ../data/IconChooserDialog.ui.h:7
msgctxt "icon-dialog"
msgid "_Icon names:"
msgstr "_Ícone nomes:"

#: ../data/IconChooserDialog.ui.h:8
msgctxt "icon-dialog"
msgid "List _standard icons only"
msgstr "Listar apenas ícones _padrão"

#: ../data/ImageChooserDialog.ui.h:1
#: ../lightdm_gtk_greeter_settings/OptionEntry.py:264
msgid "_Cancel"
msgstr "_Cancelar"

#: ../data/ImageChooserDialog.ui.h:2
#: ../lightdm_gtk_greeter_settings/OptionEntry.py:263
msgid "_OK"
msgstr "_OK"

#: ../data/IndicatorPropertiesDialog.ui.h:1
msgctxt "option-entry|indicators"
msgid "Select file..."
msgstr "Selecionar ficheiro..."

#: ../data/IndicatorPropertiesDialog.ui.h:2
msgctxt "option-entry|indicators"
msgid "Indicator properties"
msgstr "Propriedades do indicador"

#: ../data/IndicatorPropertiesDialog.ui.h:5
msgctxt "button"
msgid "_Add"
msgstr "_Adicionar"

#: ../data/IndicatorPropertiesDialog.ui.h:6
msgctxt "option-entry|indicators"
msgid "Indicator"
msgstr "Indicador"

#: ../data/IndicatorPropertiesDialog.ui.h:7
msgctxt "option-entry|indicators"
msgid "Display label"
msgstr "Etiqueta de exibição"

#: ../data/IndicatorPropertiesDialog.ui.h:8
msgctxt "option-entry|indicators"
msgid "Leave empty to use default value"
msgstr "Deixe o campo vazio para usar o valor predefinido"

#: ../data/IndicatorPropertiesDialog.ui.h:9
msgctxt "option-entry|indicators"
msgid "Display image"
msgstr "Imagem de exibição"

#: ../data/IndicatorPropertiesDialog.ui.h:10
msgctxt "option-entry|indicators"
msgid "Indicator library/service:"
msgstr "Indicador biblioteca/serviço:"

#: ../data/IndicatorPropertiesDialog.ui.h:11
msgctxt "option-entry|indicators"
msgid "Hide disabled power actions"
msgstr "Esconder ações de energia desabilitadas"

#: ../data/IndicatorPropertiesDialog.ui.h:12
msgctxt "option-entry|indicators"
msgid "Text to display:"
msgstr "Texto a exibir:"

#: ../data/IndicatorPropertiesDialog.ui.h:13
msgctxt "option-entry|indicators"
msgid "Spacer - fills the maximum available space"
msgstr "Espaçador - preenche o espaço máximo disponível"

#: ../data/IndicatorPropertiesDialog.ui.h:14
msgctxt "option-entry|indicators"
msgid "Separator - draw a separator"
msgstr "Separador - desenhar um separador"

#: ../data/MultiheadSetupDialog.ui.h:1
msgctxt "option|multihead"
msgid "Multihead setup"
msgstr "Configuração Multihead"

#: ../data/MultiheadSetupDialog.ui.h:2
msgctxt "option|multihead"
msgid "Add configuration"
msgstr "Adicionar configuração"

#: ../data/MultiheadSetupDialog.ui.h:3
msgctxt "option|multihead"
msgid "No configured monitors found"
msgstr "Não foram encontrados monitores configurados"

#: ../data/MultiheadSetupDialog.ui.h:4
msgctxt "option|multihead"
msgid "Monitor name:"
msgstr "Nome do monitor:"

#: ../data/MultiheadSetupDialog.ui.h:5
msgid "Don't leave empty"
msgstr "Não deixar em branco"

#: ../data/MultiheadSetupDialog.ui.h:6
msgctxt "option|multihead"
msgid "Add new monitor configuration"
msgstr "Adicionar nova configuração do monitor"

#: ../data/MultiheadSetupDialog.ui.h:7
msgctxt "option|multihead"
msgid "Overwrite default \"background\" option"
msgstr "Sobrescrever opção \"fundo\" predefinida"

#: ../data/MultiheadSetupDialog.ui.h:10
msgctxt "option|multihead"
msgid "Overwrite default \"user-background\" option"
msgstr "Sobrescrever opção \"fundo-utilizador\" predefinida"

#: ../data/MultiheadSetupDialog.ui.h:12
msgctxt "option|multihead"
msgid "Overwrite default \"laptop\" option"
msgstr "Sobrescrever opção \"portátil\" predefinida"

#: ../data/MultiheadSetupDialog.ui.h:13
msgctxt "option|greeter|laptop"
msgid "This monitor is laptop display (detect lid closing)"
msgstr "Este monitor é monitor dum portátil (detectado fecho de tampa)"

#: ../data/MultiheadSetupDialog.ui.h:15
msgctxt "option|multihead"
msgid ""
"<i>Note: greeter does not set monitor geometry.\n"
"You need to configure it manually.</i>"
msgstr ""

#: ../lightdm_gtk_greeter_settings/GtkGreeterSettingsWindow.py:179
msgid "You don't have permissions to change greeter configuration"
msgstr "Não tem permissões para alterar a configuração do saudador"

#: ../lightdm_gtk_greeter_settings/GtkGreeterSettingsWindow.py:183
msgid "No permissions to save configuration"
msgstr "Nenhuma permissão para gravar a configuração"

#: ../lightdm_gtk_greeter_settings/GtkGreeterSettingsWindow.py:185
#, python-brace-format
msgid ""
"It seems that you don't have permissions to write to file:\n"
"{path}\n"
"\n"
"Try to run this program using \"sudo\" or \"pkexec\""
msgstr ""
"Parece que não possui permissão para escrever no ficheiro:\n"
"{path}\n"
"\n"
"Tente executar este programa usando \"sudo\" ou \"pkexec\""

#: ../lightdm_gtk_greeter_settings/GtkGreeterSettingsWindow.py:373
msgid "<i>disabled</i>"
msgstr "<i>desativado</i>"

#: ../lightdm_gtk_greeter_settings/GtkGreeterSettingsWindow.py:375
msgid "<i>empty string</i>"
msgstr "<i>cadeia de carateres vazia</i>"

#: ../lightdm_gtk_greeter_settings/GtkGreeterSettingsWindow.py:377
msgid "<i>None</i>"
msgstr "<i>Nenhum</i>"

#: ../lightdm_gtk_greeter_settings/GtkGreeterSettingsWindow.py:401
#, python-brace-format
msgid "Value defined in file: {path}"
msgstr "Valor definido em ficheiro: {path}"

#: ../lightdm_gtk_greeter_settings/GtkGreeterSettingsWindow.py:438
#, python-brace-format
msgid "Reset to initial value: <b>{value}</b>"
msgstr "Repor para o valor inicial: <b>{value}</b>"

#: ../lightdm_gtk_greeter_settings/GtkGreeterSettingsWindow.py:450
#, python-brace-format
msgid "Reset to default value: <b>{value}</b>"
msgstr "Repor para o valor predefinido: <b>{value}</b>"

#: ../lightdm_gtk_greeter_settings/GtkGreeterSettingsWindow.py:471
#, python-brace-format
msgid "Reset to value: <b>{value}</b>"
msgstr "Repor o valor: <b>{value}</b>"

#: ../lightdm_gtk_greeter_settings/GtkGreeterSettingsWindow.py:493
#, python-brace-format
msgctxt "option|greeter|screensaver-timeout"
msgid "{count} min"
msgstr "{count} min"

#: ../lightdm_gtk_greeter_settings/GtkGreeterSettingsWindow.py:556
#, python-brace-format
msgctxt "option|greeter|allow-debugging"
msgid ""
"GtkInspector is not available on your system\n"
"Gtk version: {current} < {minimal}"
msgstr ""
"GtkInspector não está disponível no seu sistema\n"
"Versão Gtk: {current} < {minimal}"

#: ../lightdm_gtk_greeter_settings/helpers.py:175
#, python-brace-format
msgid "File not found: {path}"
msgstr "Ficheiro não encontrado: {path}"

#: ../lightdm_gtk_greeter_settings/helpers.py:199
#, python-brace-format
msgid "Failed to check permissions: {error}"
msgstr "Falha ao verificar permissões: {error}"

#: ../lightdm_gtk_greeter_settings/helpers.py:202
#, python-brace-format
msgid "Directory is not readable: {path}"
msgstr "Diretório não é legível: {path}"

#: ../lightdm_gtk_greeter_settings/helpers.py:205
#: ../lightdm_gtk_greeter_settings/helpers.py:208
#: ../lightdm_gtk_greeter_settings/helpers.py:210
#, python-brace-format
msgid "LightDM does not have permissions to read path: {path}"
msgstr ""

#: ../lightdm_gtk_greeter_settings/helpers.py:216
#, python-brace-format
msgid "Path is not a regular file: {path}"
msgstr "Caminho não é um ficheiro regular: {path}"

#: ../lightdm_gtk_greeter_settings/helpers.py:222
#: ../lightdm_gtk_greeter_settings/helpers.py:226
#: ../lightdm_gtk_greeter_settings/helpers.py:229
#, python-brace-format
msgid "LightDM does not have permissions to execute file: {path}"
msgstr ""

#: ../lightdm_gtk_greeter_settings/IconChooserDialog.py:200
msgctxt "icon-dialog"
msgid "All contexts"
msgstr "Todos os contextos"

#. separator
#: ../lightdm_gtk_greeter_settings/IconChooserDialog.py:202
msgctxt "icon-dialog"
msgid "Actions"
msgstr "Ações"

#: ../lightdm_gtk_greeter_settings/IconChooserDialog.py:203
msgctxt "icon-dialog"
msgid "Applications"
msgstr "Aplicações"

#: ../lightdm_gtk_greeter_settings/IconChooserDialog.py:204
msgctxt "icon-dialog"
msgid "Categories"
msgstr "Categorias"

#: ../lightdm_gtk_greeter_settings/IconChooserDialog.py:205
msgctxt "icon-dialog"
msgid "Devices"
msgstr "Dispositivos"

#: ../lightdm_gtk_greeter_settings/IconChooserDialog.py:206
msgctxt "icon-dialog"
msgid "Emblems"
msgstr "Emblemas"

#: ../lightdm_gtk_greeter_settings/IconChooserDialog.py:207
msgctxt "icon-dialog"
msgid "Emoticons"
msgstr "Ícones emotivos"

#: ../lightdm_gtk_greeter_settings/IconChooserDialog.py:208
msgctxt "icon-dialog"
msgid "International"
msgstr "Internacional"

#: ../lightdm_gtk_greeter_settings/IconChooserDialog.py:209
msgctxt "icon-dialog"
msgid "MIME Types"
msgstr "Tipos MIME"

#: ../lightdm_gtk_greeter_settings/IconChooserDialog.py:210
msgctxt "icon-dialog"
msgid "Places"
msgstr "Locais"

#: ../lightdm_gtk_greeter_settings/IconChooserDialog.py:211
msgctxt "icon-dialog"
msgid "Status"
msgstr "Estado"

#: ../lightdm_gtk_greeter_settings/IconEntry.py:85
#, python-brace-format
msgctxt "option-entry|icon"
msgid "Unrecognized value: {value}"
msgstr "Valor não reconhecido: {value}"

#: ../lightdm_gtk_greeter_settings/IconEntry.py:104
msgctxt "option-entry|icon"
msgid "Select icon name..."
msgstr "Selecione um nome do ícon..."

#: ../lightdm_gtk_greeter_settings/IconEntry.py:106
#, python-brace-format
msgctxt "option-entry|icon"
msgid "<b>Icon: {icon}</b>"
msgstr "<b>Ícon: {icon}</b>"

#: ../lightdm_gtk_greeter_settings/IconEntry.py:126
msgctxt "option-entry|icon"
msgid "Select file..."
msgstr "Selecionar ficheiro..."

#: ../lightdm_gtk_greeter_settings/IconEntry.py:129
#, python-brace-format
msgctxt "option-entry|icon"
msgid "<b>File: {path}</b>"
msgstr "<b>Ficheiro: {path}</b>"

#: ../lightdm_gtk_greeter_settings/IconEntry.py:131
#, python-brace-format
msgctxt "option-entry|icon"
msgid "<b>File: {path}</b> (failed to load)"
msgstr "<b>Ficheiro: {path}</b> (falha ao carregar)"

#: ../lightdm_gtk_greeter_settings/IndicatorPropertiesDialog.py:79
msgctxt "option-entry|indicators"
msgid "Use default value..."
msgstr "Usar o valor predefinido..."

#: ../lightdm_gtk_greeter_settings/IndicatorPropertiesDialog.py:81
msgctxt "option-entry|indicators"
msgid "<b>Using default value</b>"
msgstr "<b>A usar o valor predefinido</b>"

#: ../lightdm_gtk_greeter_settings/IndicatorPropertiesDialog.py:233
msgctxt "option-entry|indicators"
msgid "Path/Service field is not filled"
msgstr "O campo caminho/serviço não está preenchido"

#: ../lightdm_gtk_greeter_settings/IndicatorPropertiesDialog.py:237
#, python-brace-format
msgctxt "option-entry|indicators"
msgid ""
"Indicator \"{name}\" is already in the list.\n"
"It will be overwritten."
msgstr ""
"Indicador \"{name}\" já existe na lista.\n"
"Ele será reescrito."

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:131
msgctxt "option-entry|indicators|name"
msgid "External library/service"
msgstr "Biblioteca/serviço externo"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:132
msgctxt "option-entry|indicators|name"
msgid "Spacer"
msgstr "Espaçador"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:133
msgctxt "option-entry|indicators|name"
msgid "Separator"
msgstr "Separador"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:134
msgctxt "option-entry|indicators|name"
msgid "Text"
msgstr "Texto"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:135
msgctxt "option-entry|indicators|name"
msgid "Clock"
msgstr "Relógio"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:136
msgctxt "option-entry|indicators|name"
msgid "Host name"
msgstr "Nome do anfitrião"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:137
msgctxt "option-entry|indicators|name"
msgid "Keyboard layout"
msgstr "Esquema de teclado"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:138
msgctxt "option-entry|indicators|name"
msgid "Sessions menu"
msgstr "Menu de sessões"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:139
msgctxt "option-entry|indicators|name"
msgid "Languages menu"
msgstr "Menu de linguagens"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:140
msgctxt "option-entry|indicators|name"
msgid "Accessibility menu"
msgstr "Menu de acessibilidade"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:141
msgctxt "option-entry|indicators|name"
msgid "Power menu"
msgstr "Menu de energia"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:148
msgctxt "option-entry|indicators|tooltip"
msgid "Spacer"
msgstr "Espaçador"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:149
msgctxt "option-entry|indicators|tooltip"
msgid "Separator"
msgstr "Separador"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:150
msgctxt "option-entry|indicators|tooltip"
msgid "Custom text or/and image"
msgstr "Texto e/ou imagem personalizado"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:151
msgctxt "option-entry|indicators|tooltip"
msgid "Host name"
msgstr "Nome do anfitrião"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:152
msgctxt "option-entry|indicators|tooltip"
msgid "Clock"
msgstr "Relógio"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:153
msgctxt "option-entry|indicators|tooltip"
msgid "Layout indicator"
msgstr "Indicador de exibição"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:155
msgctxt "option-entry|indicators|tooltip"
msgid "Sessions menu (xfce, unity, gnome etc.)"
msgstr "Menu de sessões (xfce, unity, gnome etc.)"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:156
msgctxt "option-entry|indicators|tooltip"
msgid "Languages menu"
msgstr "Menu de linguagens"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:157
msgctxt "option-entry|indicators|tooltip"
msgid "Accessibility menu"
msgstr "Menu de acessibilidade"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:158
msgctxt "option-entry|indicators|tooltip"
msgid "Power menu"
msgstr "Menu de energia"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:583
msgctxt "option-entry|indicators"
msgid "Reset to _defaults"
msgstr "Repor _predefinições"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:585
msgctxt "option-entry|indicators"
msgid "Display _label"
msgstr "Exibir _etiqueta"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:587
msgctxt "option-entry|indicators"
msgid "Display _image"
msgstr "Exibir _imagem"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:589
msgctxt "option-entry|indicators"
msgid "_Remove"
msgstr "_Remover"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:689
msgctxt "option-entry|indicators"
msgid "Show unused items"
msgstr "Mostrar itens não utilizados"

#: ../lightdm_gtk_greeter_settings/IndicatorsEntry.py:693
msgctxt "option-entry|indicators"
msgid "Predefined templates:"
msgstr "Modelos predefinidos:"

#: ../lightdm_gtk_greeter_settings/OptionEntry.py:265
msgctxt "option|StringPathEntry"
msgid "Select path"
msgstr "Selecionar caminho"

#: ../com.ubuntu.pkexec.lightdm-gtk-greeter-settings.policy.in.h:1
msgid ""
"Authentication is required to run Settings editor for LightDM GTK+ Greeter"
msgstr ""
"A autenticação é necessária para executar o editor Configurações para o "
"saudador LightDM GTK+"
